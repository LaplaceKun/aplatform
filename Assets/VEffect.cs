﻿using UnityEngine;
using System.Collections;

public class VEffect : MonoBehaviour {

   private bool Check;
   public LevelBuilder LB;
	// Use this for initialization
    void Start() {
        transform.rotation = Quaternion.Euler(0, 0, 90);
    }
    void OnTriggerStay(Collider col) {
            Check = true;         
    }
    void FixedUpdate() {
        if (Check == true) {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 15);
        } else {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 90), Time.deltaTime * 15);
        }
    }
}
