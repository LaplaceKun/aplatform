﻿using UnityEngine;
using System.Collections;
public class LevelBuilder : MonoBehaviour {
    public GameObject Plane;
    public Transform StartBlock;
    public int Lenght;
    public int MaxBlock;
    public int StartPlatform;
    private Quaternion BlockAngle = Quaternion.Euler(0, 0, 0);
    private Vector3 OffsetX;
    void Start() {
        while (StartPlatform < 10) {
            BuildLine();
            StartPlatform++;
            Lenght++;
        }
    }
    void Update() {
        if (Lenght == MaxBlock) {
            StartBlock.position += new Vector3(0, 0, 2);
            Lenght = 0;
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            while (Lenght < MaxBlock) {
                BuildLine();
                Lenght++;
            }
        } else {
            OffsetX = new Vector3(2, 0, 0);
        }
    }
    void BuildLine() {
        Instantiate(Plane, StartBlock.position + OffsetX, BlockAngle);
        OffsetX += new Vector3(2, 0, 0);
    }
}
