﻿using UnityEngine;
using System.Collections;

public class FollowRotate : MonoBehaviour {
	
public Transform player;
public float Speed = 60f;
	
	void FixedUpdate() {
		Vector3 RotateVector = Vector3.Cross((player.position - transform.position),transform.forward);
		rigidbody.AddTorque(RotateVector*Speed,ForceMode.Acceleration);
	}
}